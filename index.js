const form = document.getElementById("todo-form");
const todoInput = document.querySelector("#todo");
const todoList = document.querySelector(".list-group");
const cardbody1 = document.querySelectorAll(".card-body")[0];
const cardbody2 = document.querySelectorAll(".card-body")[1];
const filter = document.getElementById("filter");
const clearalldocuments = document.getElementById("clear-todos");


EventListen();

//Tüm Eventleri Dinlemek icin Function
function EventListen() {
    form.addEventListener("submit", addFunction);
    document.addEventListener("DOMContentLoaded", loadAllValuestoUI);
    cardbody2.addEventListener("click", deleteElementonUI);
    filter.addEventListener("keyup", filterText)
    clearalldocuments.addEventListener("click", clearall)
}

function clearall(e) {
    if (confirm("Silmek istediğinize emin misiniz ?")) {
        // todoList.innerHTML=""; 1. yöntem ama en kısa ve yavaş olanı
        while (todoList.firstElementChild != null) {
            todoList.removeChild(todoList.firstElementChild);
        }
        localStorage.removeItem("localvalues");
    }
}



// Ekleme Functionu
function addFunction(e) {
    let consolevalue = false;
    let list = document.querySelectorAll(".list-group-item");
    let newElement = todoInput.value.trim(); // Value alıyor

    if (newElement === "") {
        alertMessage("danger", "Lütfen Bir mesaj yazınız");
    }
    else {
        list.forEach(function (e) {
            let text = e.textContent;

            if (text.toLowerCase() == newElement.toLowerCase()) {
                consolevalue = true;
            }

        })
        if (consolevalue == false) {
            alertMessage("success", "Eleman eklendi")
            addUI(newElement);
            addLocalStorage(newElement);
        }
        else {
            alertMessage("warning", "Eleman önceden eklenmiştir.")
        }
    }
    e.preventDefault();
    todoInput.value = ""//Eklendikten sonra Input kısmını boşaltmak için  

}

function deleteElementonUI(e) {
    if (e.target.className === "fa fa-remove") {
        deleteElementLocalStorage(e.target.parentElement.parentElement.textContent);
        e.target.parentElement.parentElement.remove();
        alertMessage("success", "Silme işlemi başarılı");
    }
};

function deleteElementLocalStorage(deleteText) {
    let localvalues = getLocalStorageValue();

    localvalues.forEach(function (e, index) {
        if (e === deleteText) {
            localvalues.splice(index, 1); //splice Arrayden remove methodu
        }

        localStorage.setItem("localvalues", JSON.stringify(localvalues));
    })
}

function filterText(e) {
    let content = e.target.value.toLowerCase()
    let listItems = document.querySelectorAll(".list-group-item");

    listItems.forEach(function (listItem) {
        const text = listItem.textContent.toLowerCase();
        if (text.indexOf(content) === -1) {
            //Bulamadı
            listItem.setAttribute("style", "display : none !important"); //d-flex butonların sağ yaslanmasını sağlıyor display none engilliyor içinde block var onu kapatmak için important
        }
        else {
            listItem.setAttribute("style", "display : block");
        }
    })
}


//Alert MESAJI
function alertMessage(type, message) {
    //https://getbootstrap.com/docs/4.0/components/alerts/

    const alert = document.createElement("div");
    alert.className = `alert alert-${type}`
    alert.textContent = message;
    // alert.style ="display : none";  // Sonradan uğraş
    cardbody1.appendChild(alert);

    setTimeout(function () {
        alert.remove();
    }, 1000);
}

//Localden verilerimizi almak için
function getLocalStorageValue() { // Local Storage dan verilerimizi alıyor
    let localvalues;
    if (localStorage.getItem("localvalues") === null) {
        localvalues = []
    }
    else {
        localvalues = JSON.parse(localStorage.getItem("localvalues"))
    }

    return localvalues // Sonradan kullanmak için
}

//Locale verileri eklemek için
function addLocalStorage(newElement) {
    let localvalues = getLocalStorageValue();
    localvalues.push(newElement);
    localStorage.setItem("localvalues", JSON.stringify(localvalues));
}

function loadAllValuestoUI() {
    let localvalues = getLocalStorageValue();

    localvalues.forEach(function (e) {
        addUI(e);
    });

}


function addUI(newElement) {

    /* ÖRNEK 
    <li class="list-group-item d-flex justify-content-between">
                            Todo 1
                            <a href = "#" class ="delete-item">
                                <i class = "fa fa-remove"></i>
                            </a>

                        </li> */
    const addListitem = document.createElement("li");
    const addListitem2 = document.createElement("a");
    //Alt child oluşturma
    addListitem2.href = "#";
    addListitem2.className = "delete-item";
    addListitem2.innerHTML = "<i class = 'fa fa-remove'></i>";
    //Üst child oluşturma
    addListitem.className = "list-group-item d-flex justify-content-between";
    //console.log(addListitem); Kontrol ekleme olumlu mu diye

    addListitem.appendChild(document.createTextNode(newElement));
    //Altı üstün elemenanı yapma
    addListitem.appendChild(addListitem2);

    //<ul> altına ekleme Parent'a geçirme parent = toDoList
    todoList.appendChild(addListitem);


    /******************************************************************************************************** */
    //BUDA BAŞKA BİR ŞEKİLDE YAZILIŞ HALİ (YAVAŞ OLUYOR DİĞERİNE GÖRE)
    /* const listItem=`<li class="list-group-item d-flex justify-content-between">
                           ${newElement}
                           <a href = "#" class ="delete-item">
                           <i class = "fa fa-remove"></i>
                           </a>
                            </li>`
                           
                           todoList.innerHTML +=listItem;*/
    /******************************************************************************************************** */

}